@echo off
setlocal

call E:\lcbo\LCO_BinTag\BinTag.bat

set CUR_DIR=%~dp0
REM REMOVE TRAILING BACKSLACH IF PRESENT
IF %CUR_DIR:~-1%==\ SET CUR_DIR=%CUR_DIR:~0,-1%

set LOG_DIR=%CUR_DIR%\LOGS
IF NOT EXIST %LOG_DIR% MKDIR %LOG_DIR%
set LOG_FILE=%LOG_DIR%\%~nx0_%SCRIPT_DTTM%.log

echo %SCRIPT_DTTM% START %TIME% > %LOG_FILE%

REM ***********MAIN CONTENT START***************


sqlplus %user%/%passwd%@%DB% @%CUR_DIR%\BinTagPriceExtract.sql >> %LOG_FILE%

findstr /i Error %LOG_FILE%
If %errorlevel%==0 echo %date%  %time% Error Extracting Price   >> %LOG_FILE% & Goto Error

call %CUR_DIR%\BinTagPriceFTP.bat >> %LOG_FILE%

findstr /i Error %LOG_FILE%
If %errorlevel%==0 echo %date%  %time% Error FTP Price   >> %LOG_FILE% & Goto Error


GOTO END
REM ***********MAIN CONTENT END ****************

:Error
cd %CUR_DIR%
REM copy /Y %LOG_FILE%  %CUR_DIR%\email.msg
c:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe "%CUR_DIR%\SendMail.ps1"  

:END
echo %SCRIPT_DTTM% FINISH %TIME% >> %LOG_FILE%
endlocal

REM exit
