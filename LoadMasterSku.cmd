@echo off
setlocal

call E:\lcbo\LCO_BinTag\BinTag.bat

set CUR_DIR=%~dp0
REM REMOVE TRAILING BACKSLACH IF PRESENT
IF %CUR_DIR:~-1%==\ SET CUR_DIR=%CUR_DIR:~0,-1%

set LOG_DIR=%CUR_DIR%\LOGS
IF NOT EXIST %LOG_DIR% MKDIR %LOG_DIR%
set LOG_FILE=%LOG_DIR%\%~nx0_%SCRIPT_DATE%.log

set CONTROL_FILE=%CUR_DIR%\LoadMasterSku.ctl
set WRK_DIR=%CUR_DIR%\WRK
set BAK_DIR=%CUR_DIR%\ARCH
set GRPCD_DIR=%TFR_OLD%\download
set LDR_DIR=%ORACLE_HOME%\BIN

IF Not Exist %WRK_DIR% mkdir %WRK_DIR%
IF Not Exist %BAK_DIR% mkdir %BAK_DIR% 

echo %SCRIPT_DTTM% START %TIME% >> %LOG_FILE%

If Not Exist MasterSku.csv echo Input file does not exist >> %LOG_FILE% & Goto :EndBatch
  
If Exist %CUR_DIR%\MasterSku.csv echo %date%  %time% Loading master sku is currently running!!! >> %LOG_FILE% 
  
%LDR_DIR%\sqlldr %user%/%passwd%@%DB% control=%CONTROL_FILE% data=MasterSku.csv log=%LOG_FILE%.ldr rows=1000000 bindsize=600000 errors=99999 >> %LOG_FILE%

set rc=%errorlevel%
If %rc% NEQ 0 echo %date%  %time% Error Loading Table Returned Code = %rc%   >> %LOG_FILE% 
echo %date%  %time% Load returned ErrCode = 0  >> %LOG_FILE%



:STANDBY
echo %date% %time% %0 - Run on STANDBY. Nothing to Process. >> %LOG_FILE%

:EndBatch
echo %SCRIPT_DTTM% FINISH %TIME% >> %LOG_FILE%