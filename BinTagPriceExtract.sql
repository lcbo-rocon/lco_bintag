SPOOL E:\lcbo\LCO_BinTag\BinTagPrice.txt
set term off
set feedback off
set heading off
set trimspool on
set pages 0
set linesize 500
set feed off
set echo off
set verify off
SELECT 'ITEMID|REGPRICE|SALEPRICE|STARTDATE|STOPDATE|DIFF|CHANGETYPE'
FROM DUAL
/
SELECT 
            PR.ITEM_NUMBEr||'|'||
            nvl(item.retail_price,0)||'|'||
            nvl(item.retail_price - sl.approved,0)||'|'||
             to_char(SL.START_DATE,'YYYY-MM-DD')||'|'||
            to_char(SL.END_DATE,'YYYY-MM-DD')||'|'||
            nvl(SL.Approved,0)||'|'||
            'SS'
        FROM pts_promotion_applications pr,
            pts_super_sale_lto sl,
            pts_promotions p,
            b2b_bintag_item item
      WHERE     PR.PROMOTION_APP_ID = SL.PROMOTION_APP_ID
            AND PR.PROMO_SSL_ID = P.PROMOTION_ID
            AND P.STATUS_CODE = 'A'
            AND TO_CHAR(SL.START_DATE,'YYYYMMDD') = TO_CHAR(SYSDATE+7,'YYYYMMDD')  
            AND PR.item_number = item.itemid
   UNION 
      SELECT 
          PA.ITEM_NUMBER||'|'||
          nvl(ITEM.RETAIL_PRICE,0)||'|'||
          nvl(ITEM.RETAIL_PRICE-LT.APPROVED,0)||'|'||
          TO_CHAR(LC.LTO_PERIOD_BEGIN,'YYYY-MM-DD')||'|'||
          TO_CHAR(LC.LTO_PERIOD_END,'YYYY-MM-DD')||'|'||
          LT.APPROVED||'|'||
          'LTO'
      FROM pts_promotion_applications pa,
          pts_promotions pr,
          PTS_LIMITED_TIME_OFFER lt,
          PTS_INV_LTO_CALENDAR lc,
          b2b_bintag_item item
    WHERE     PA.PROMO_LTO_ID = PR.PROMOTION_ID
          AND PR.PROMOTION_ID = LT.PROMOTION_ID
          AND Pa.PROMOTION_PERIOD = LC.PROMOTION_PERIOD
          --AND I.ITEM_NUMBER = PA.ITEM_NUMBER
          AND PA.STATUS_CODE = 'A'
          AND PR.STATUS_CODE = 'A'
          AND TO_CHAR(LC.LTO_PERIOD_BEGIN,'YYYYMMDD') = TO_CHAR(SYSDATE+7, 'YYYYMMDD')
          AND PA.ITEM_NUMBER = ITEM.ITEMID
/
SPOOL OFF
/
quit

