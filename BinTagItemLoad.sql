DROP TABLE B2B_BINTAG_ITEM_PRE
/
RENAME  B2B_BINTAG_ITEM TO B2B_BINTAG_ITEM_PRE
/
CREATE TABLE B2B_BINTAG_ITEM AS 
SELECT I.ITEM_NUMBER AS ItemID, 
         ri.list_delist_code,
          ri.listing_date,
          ri.upc_number as UPC,
          i.Category_ID as CategoryID, 
          T.Description_eng as CategoryDesc,
          I.Country_id as DepartmentID,
          UPPER (C.DESCRIPTION_ENG) AS DepartmentDesc,
          UPPER (C.DESCRIPTION_FRE) AS UserDefined1_department_french,
          UPPER (I.PUBLICATION_NAME_ENG) AS Name,
          UPPER (I.PUBLICATION_NAME_FRE) AS AlternateName_french,
  --         ps.description_eng AS F1_Style_english,
  --        ps.description_fre AS F2_styLe_french,
                 '('||RI.Alcohol_percent||' %,'||RI.SUGAR_GM_PER_LTR || ' g/L)' as F3_sugar_alcohol,
               DECODE (RI.BOTTLES_PER_PACK,
                  1, PM.DESCRIPTION_ENG,
                  PM.DESCRIPTION_ENG_PLURAL)
             AS F7_PACKAGE_TYPE,
                  prc.retl_prce_amt AS retail_price,
          prc.dpst_amt AS user_price1,
                   DECODE (
             RI.BOTTLES_PER_PACK,
             1, RI.UNIT_VOLUME || ' ' || RI.UNIT_OF_MEASURE,
                RI.BOTTLES_PER_PACK
             || ' x '
             || ROUND (RI.UNIT_VOLUME / RI.BOTTLES_PER_PACK)
             || ' '
             || RI.UNIT_OF_MEASURE)
             AS UOMSIZE,
          TP.DESCRIPTION_ENG F8_taste_profile_english,
       TP.DESCRIPTION_FRE F9_taste_profile_french,
        'A' as updatestatus,
        to_char(sysdate,'YYYY-MM-DD') as modifieddate
   --  SELECT COUNT(DISTINCT i.ITEM_NUMBER)
  -- SELECT COUNT(*)
    FROM IA_ITEM I
       INNER JOIN REF_ITEM_DIMENSION RI
           on RI.ITEM_NUMBER = I.ITEM_NUMBER
            AND RI.LIST_DELIST_CODE in (2,3,4)
           -- AND RI.BRAND_CODE NOT BETWEEN '904650' AND '904659' 
        Left join    IA_PRODUCT_COUNTRY C 
           on C.COUNTRY_ID = I.COUNTRY_ID
        Left join   IA_PRODUCT_CATEGORY T 
           on T.CATEGORY_ID = I.CATEGORY_ID
         Left join  IA_PUB_PACKAGING_MATERIAL PM
           on PM.PUB_PACKAGING_MATERIAL_ID = RI.PACKAGING_MATERIAL_CODE
         INNER JOIN prcadmin.pricing_master@lcsg.world PRC
           ON PRC.ITEM_NO = I.ITEM_NUMBER
           AND PRC.ITEM_TYPE_CD = 'REG'
          INNER JOIN  ( select item_no, max(prc.efct_prce_dt) max_date
               FROM 
               prcadmin.pricing_master@lcsg.world prc
              WHERE     prc.EFCT_PRCE_DT <= CURRENT_DATE 
                     AND prc.item_type_cd = 'REG'
               --     AND remarks not like '%LTO%'
           GROUP BY prc.item_no) maxdate 
           ON MAXDATE.ITEM_NO = I.ITEM_NUMBER
          LEFT JOIN IA_PRODUCT_TASTE_PROFILE TP
           ON TP.TASTE_PROFILE_ID = I.TASTE_PROFILE_ID
     --     LEFT JOIN IA_PRODUCT_STYLE PS
     --      ON PS.STYLE_ID = I.STYLE_ID
  where 
         prc.item_no = maxdate.item_no
          AND prc.efct_prce_dt = maxdate.max_date
       --  and prc.dpst_amt =  0 
         --and tp.description_eng is null
         --and i.item_number = 18
/
          
         -- SELECT * FROM B2B_BINTAG_ITEM;
          
  UPDATE B2B_BINTAG_ITEM item
    SET  retail_price = retail_price +   
         (select approved
        FROM pts_promotion_applications pr,
            pts_super_sale_lto sl,
            pts_promotions p
           WHERE     PR.PROMOTION_APP_ID = SL.PROMOTION_APP_ID
            AND PR.PROMO_SSL_ID = P.PROMOTION_ID
            AND P.STATUS_CODE = 'A'
            AND SL.START_DATE <= SYSDATE 
            AND SL.END_DATE >= sysdate
            AND PR.item_number = item.itemid)
   where exists ( 
          select *
        FROM pts_promotion_applications pr,
            pts_super_sale_lto sl,
            pts_promotions p
           WHERE     PR.PROMOTION_APP_ID = SL.PROMOTION_APP_ID
            AND PR.PROMO_SSL_ID = P.PROMOTION_ID
            AND P.STATUS_CODE = 'A'
            AND SL.START_DATE <= SYSDATE 
            AND SL.END_DATE >= sysdate
            AND PR.item_number = item.itemid)
/   
   update B2B_BINTAG_ITEM ITEM
   SET RETAIL_PRICE = RETAIL_PRICE + (
   SELECT 
          LT.APPROVED 
      FROM pts_promotion_applications pa,
          pts_promotions pr,
          PTS_LIMITED_TIME_OFFER lt,
          PTS_INV_LTO_CALENDAR lc
     WHERE     PA.PROMO_LTO_ID = PR.PROMOTION_ID
          AND PR.PROMOTION_ID = LT.PROMOTION_ID
          AND Pa.PROMOTION_PERIOD = LC.PROMOTION_PERIOD
          --AND I.ITEM_NUMBER = PA.ITEM_NUMBER
          AND PA.STATUS_CODE = 'A'
          AND PR.STATUS_CODE = 'A'
          AND LC.LTO_PERIOD_BEGIN <= SYSDATE
          AND LC.LTO_PERIOD_END >= SYSDATE 
          AND PA.ITEM_NUMBER = ITEM.ITEMID)
   WHERE EXISTS (SELECT * 
            FROM pts_promotion_applications pa,
          pts_promotions pr,
          PTS_LIMITED_TIME_OFFER lt,
          PTS_INV_LTO_CALENDAR lc
     WHERE     PA.PROMO_LTO_ID = PR.PROMOTION_ID
          AND PR.PROMOTION_ID = LT.PROMOTION_ID
          AND Pa.PROMOTION_PERIOD = LC.PROMOTION_PERIOD
          --AND I.ITEM_NUMBER = PA.ITEM_NUMBER
          AND PA.STATUS_CODE = 'A'
          AND PR.STATUS_CODE = 'A'
          AND LC.LTO_PERIOD_BEGIN <= SYSDATE
          AND LC.LTO_PERIOD_END >= SYSDATE 
          AND PA.ITEM_NUMBER = ITEM.ITEMID)
/

commit
/
quit

