SPOOL E:\lcbo\LCO_BinTag\BinTagRegPrice.txt
set term off
set feedback off
set heading off
set trimspool on
set pages 0
set linesize 500
set feed off
set echo off
set verify off
SELECT 'ITEMID|REGPRICE|STARTDATE'
FROM DUAL
/
select itemid||'|'||retl_prce_amt||'|'||to_char(efct_prce_dt,'YYYY-MM-DD')
from prcadmin.pricing_master@lcsg.world prc 
 inner join b2b_bintag_item 
  on itemid = prc.item_no
where 
       to_char(prc.EFCT_PRCE_DT,'YYYYMMDD') = to_char(CURRENT_DATE + 7,'YYYYMMDD')
   AND item_type_cd = 'REG'
   AND retl_prce_amt <> retail_price
   AND not exists (SELECT *
         FROM pts_promotion_applications pr,
            pts_super_sale_lto sl,
            pts_promotions p
         WHERE     PR.PROMOTION_APP_ID = SL.PROMOTION_APP_ID
            AND PR.PROMO_SSL_ID = P.PROMOTION_ID
            AND P.STATUS_CODE = 'A'
            AND TO_CHAR(SL.START_DATE,'YYYYMMDD') = TO_CHAR(prc.EFCT_PRCE_DT,'YYYYMMDD') 
            AND PR.ITEM_NUMBER = PRC.ITEM_NO )
   AND not exists ( 
      SELECT *
       FROM pts_promotion_applications pa,
          pts_promotions pr,
          PTS_LIMITED_TIME_OFFER lt,
          PTS_INV_LTO_CALENDAR lc
        WHERE     PA.PROMO_LTO_ID = PR.PROMOTION_ID
          AND PR.PROMOTION_ID = LT.PROMOTION_ID
          AND Pa.PROMOTION_PERIOD = LC.PROMOTION_PERIOD
          AND PA.STATUS_CODE = 'A'
          AND PR.STATUS_CODE = 'A'
          AND TO_CHAR(LC.LTO_PERIOD_BEGIN,'YYYYMMDD') = TO_CHAR(prc.EFCT_PRCE_DT, 'YYYYMMDD')
          AND PA.ITEM_NUMBER = PRC.ITEM_NO)
union 
select itemid||'|'||retl_prce_amt||'|'||to_char(efct_prce_dt,'YYYY-MM-DD')
from prcadmin.pricing_master@lcsg.world prc 
 inner join b2b_bintag_item 
  on itemid = prc.item_no
where 
       to_char(prc.EFCT_PRCE_DT,'YYYYMMDD') > to_char(CURRENT_DATE,'YYYYMMDD') 
   and to_char(prc.EFCT_PRCE_DT,'YYYYMMDD') < to_char(CURRENT_DATE + 7,'YYYYMMDD')
   and prc.last_mdfy_dttm >= CURRENT_DATE - 1
   AND item_type_cd = 'REG'
--   AND retl_prce_amt <> retail_price
    AND not exists (SELECT *
         FROM pts_promotion_applications pr,
            pts_super_sale_lto sl,
            pts_promotions p
      WHERE     PR.PROMOTION_APP_ID = SL.PROMOTION_APP_ID
            AND PR.PROMO_SSL_ID = P.PROMOTION_ID
            AND P.STATUS_CODE = 'A'
            AND TO_CHAR(SL.START_DATE,'YYYYMMDD') = TO_CHAR(prc.efct_prce_dt,'YYYYMMDD') 
            AND PR.ITEM_NUMBER = PRC.ITEM_NO )
   AND not exists ( 
      SELECT *
       FROM pts_promotion_applications pa,
          pts_promotions pr,
          PTS_LIMITED_TIME_OFFER lt,
          PTS_INV_LTO_CALENDAR lc
    WHERE     PA.PROMO_LTO_ID = PR.PROMOTION_ID
          AND PR.PROMOTION_ID = LT.PROMOTION_ID
          AND Pa.PROMOTION_PERIOD = LC.PROMOTION_PERIOD
          --AND I.ITEM_NUMBER = PA.ITEM_NUMBER
          AND PA.STATUS_CODE = 'A'
          AND PR.STATUS_CODE = 'A'
          AND TO_CHAR(LC.LTO_PERIOD_BEGIN,'YYYYMMDD') = TO_CHAR(prc.efct_prce_dt, 'YYYYMMDD')
          AND PA.ITEM_NUMBER = PRC.ITEM_NO)
/
SPOOL OFF
/
quit

