SPOOL E:\lcbo\LCO_BinTag\BinTagItem.txt
set term off
set feedback off
set heading off
set trimspool on
set pages 0
set linesize 500
set feed off
set echo off
set verify off
SELECT 'ITEMID|UPC|CATEGORYID|CATEGORYDESC|DEPARTMENTID|DEPARTMENTDESC|USERDEFINED1_DEPARTMENT_FRENCH|NAME|ALTERNATENAME_FRENCH|F1_STYLE_ENGLISH|F2_STYLE_FRENCH|F3_SUGAR_ALCOHOL|F7_PACKAGE_TYPE|RETAIL_PRICE|USER_PRICE1|UOMSIZE|F8_TASTE_PROFILE_ENGLISH|F9_TAST_PROFILE_FRENCH|UPDATESTATUS|MODIFIEDDATE'
FROM DUAL
/
SELECT ITEMID||'|'||
         UPC||'|'||
        CATEGORYID||'|'|| 
        CATEGORYDESC||'|'|| 
        DEPARTMENTID||'|'|| 
       DEPARTMENTDESC||'|'||
       USERDEFINED1_DEPARTMENT_FRENCH||'|'||
        NAME||'|'||
        ALTERNATENAME_FRENCH||'|'||
        '||'||
        F3_SUGAR_ALCOHOL||'|'||
       F7_PACKAGE_TYPE||'|'||
       RETAIL_PRICE||'|'||
       USER_PRICE1||'|'||
       UOMSIZE||'|'||
       F8_TASTE_PROFILE_ENGLISH||'|'||
       F9_TASTE_PROFILE_FRENCH||'|'||
       'A'||'|'|| 
        TO_CHAR(SYSDATE, 'YYYY-MM-DD')
  FROM B2B_BINTAG_ITEM A 
  WHERE NOT EXISTS (SELECT * FROM B2B_BINTAG_ITEM_PRE WHERE ITEMID = A.ITEMID)
UNION
SELECT ITEMID||'|'||
         UPC||'|'||
        CATEGORYID||'|'|| 
        CATEGORYDESC||'|'|| 
        DEPARTMENTID||'|'|| 
       DEPARTMENTDESC||'|'||
       USERDEFINED1_DEPARTMENT_FRENCH||'|'||
        NAME||'|'||
        ALTERNATENAME_FRENCH||'|'|| 
           '||'||
       F3_SUGAR_ALCOHOL||'|'||
       F7_PACKAGE_TYPE||'|'||
       RETAIL_PRICE||'|'||
       USER_PRICE1||'|'||
       UOMSIZE||'|'||
       F8_TASTE_PROFILE_ENGLISH||'|'||
       F9_TASTE_PROFILE_FRENCH||'|'||
       'U'||'|'|| 
        TO_CHAR(SYSDATE, 'YYYY-MM-DD')
  FROM B2B_BINTAG_ITEM A
  WHERE EXISTS (SELECT * FROM B2B_BINTAG_ITEM_PRE WHERE ITEMID = A.ITEMID
                   AND (UPC <> A.UPC
                   OR CATEGORYID <> A.CATEGORYID
                   OR CATEGORYDESC <> A.CATEGORYDESC
                   OR DEPARTMENTID <> A.DEPARTMENTID
                   OR DEPARTMENTDESC <> A.DEPARTMENTDESC
                   OR USERDEFINED1_DEPARTMENT_FRENCH <> A.USERDEFINED1_DEPARTMENT_FRENCH
                   OR NAME <> A.NAME 
                   OR ALTERNATENAME_FRENCH <> A.ALTERNATENAME_FRENCH
                   OR F3_SUGAR_ALCOHOL <> A.F3_SUGAR_ALCOHOL
                   OR F7_PACKAGE_TYPE <> A.F7_PACKAGE_TYPE
               --    OR RETAIL_PRICE <> A.RETAIL_PRICE
                   OR USER_PRICE1 <> A.USER_PRICE1
                   OR UOMSIZE <> A.UOMSIZE
                   OR F8_TASTE_PROFILE_ENGLISH <> A.F8_TASTE_PROFILE_ENGLISH
                   OR F9_TASTE_PROFILE_FRENCH <> A.F9_TASTE_PROFILE_FRENCH
                   Or ITEMID IN (15367)
                       )
              )
UNION
SELECT ITEMID||'|'||
         UPC||'|'||
        CATEGORYID||'|'|| 
        CATEGORYDESC||'|'|| 
        DEPARTMENTID||'|'|| 
       DEPARTMENTDESC||'|'||
       USERDEFINED1_DEPARTMENT_FRENCH||'|'||
        NAME||'|'||
        ALTERNATENAME_FRENCH||'|'|| 
           '||'||
     F3_SUGAR_ALCOHOL||'|'||
       F7_PACKAGE_TYPE||'|'||
       RETAIL_PRICE||'|'||
       USER_PRICE1||'|'||
       UOMSIZE||'|'||
       F8_TASTE_PROFILE_ENGLISH||'|'||
       F9_TASTE_PROFILE_FRENCH||'|'||
       'D'||'|'|| 
        TO_CHAR(SYSDATE, 'YYYY-MM-DD')
  FROM B2B_BINTAG_ITEM_PRE A
  WHERE NOT EXISTS (SELECT * FROM B2B_BINTAG_ITEM WHERE ITEMID = A.ITEMID)  
/
SPOOL OFF
/
quit

