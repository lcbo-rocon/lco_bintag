@ECHO OFF
REM This script is used to send(FTP) the BinTagItem.txt file to Lexmark server
SETLOCAL
call E:\lcbo\LCO_BinTag\BinTag.bat

set rootDir=E:\lcbo\LCO_BinTag
set startDir=%rootDir%
set logDir=%rootDir%\LOGS
set sendDir=%rootDir%\
set workDir=%rootDir%\
set backupDir=%rootDir%\ARCH

E:
cd %rootDir%

for /f %%a in ('perl getToday.pl yyyymmdd' ) do set YYYYMMDD=%%a

set currentdate=%YYYYMMDD%


set logfile=%logDir%\SendTolexmarkFTP_Skulist.%currentdate%.log

echo ********************************** >> %logfile%
echo %date% %time%  SendTolexmarkFTP started >> %logfile%

REM uploading file to lexmark folder
set remoteDir=/
set fileName=BinTagSkulist.txt
set remotefileName=StoreSku_%currentdate%.txt
set ftpResponseFile=%rootDir%\LOGS\sftplexmarkResponse_storesku.log
set ftpAPTFile=%rootDir%\LOGS\SFTPlexmark_storesku.log
set sftpServer=%lexmarkFTPServer%

REM CHECK if there are files to be sent

set sendFiles=%sendDir%\%fileName%
set cnt=0
set fileSent=
set filesFound=0
set errorsFound=0

if not exist "%sendDir%\%fileName%" goto NOFILES

dir %sendDir%\%fileName%  >>%logfile%

REM copy /y %sendDir%\%fileName% %workDir%\. & del /q %sendDir%\%fileName% >>%logfile%

REM dir %workDir%\%fileName%  >>%logfile%

REM echo file copied.

echo Working uploading file to server: %lexmarkFTPServer% >> %logfile%

echo "changing to root directory %rootDir%" >> %logfile%
cd %rootDir% >> %logfile%

echo creating %ftpResponseFile% >> %logfile%
echo. > %ftpResponseFile%

echo files will be uploaded >> %logfile%
set sftpURL=sftp://%ftpUID%:%ftpPWD%@%sftpServer% -hostkey=*
REM set sftpURL=sftp://%ftpUID%@%sftpServer% -privatekey=%privateKeyFile%

echo creating SFTP command file >> %logfile%
echo option batch abort >%ftpAPTFile%
echo option confirm off >>%ftpAPTFile%
echo open %sftpURL% >>%ftpAPTFile%
echo cd %remoteDir% >> %ftpAPTFile%
echo lcd %workDir% >> %ftpAPTFile%
echo bin >> %ftpAPTFile%
echo put  %FileName% %remotefileName% >> %ftpAPTFile%
echo close >> %ftpAPTFile%
echo exit >> %ftpAPTFile%

echo putting files %FileName% >> %logfile%
%rootDir%\winscp.com /script:%ftpAPTFile% > %ftpResponseFile%

copy /y %sendDir%\%fileName% %backupDir%\%remotefileName% & del /q %sendDir%\%fileName% >>%logfile%

dir %workDir%\%fileName%  >>%logfile%

echo file copied.

findstr /i /c:"Error" %ftpResponseFile% >> %logfile%
echo looking for error in response file error level %errorlevel% >> %logfile%

IF %errorlevel%==0 (
 	set errorsFound=1
)

findstr /i /c:"No file matching" %ftpResponseFile%

echo looking for files in response file error level %errorlevel% >> %logfile%

IF %errorlevel%==1 (
	set filesFound=1
)
REM disabling file check since there is not needed
set filesFound=1

findstr /i /c:"Transfer" %ftpResponseFile%
echo error level %errorlevel% >> %logfile%


IF %errorlevel%==1 (

	echo error no transfer message found uploading files, see %ftpResponseFile% and %logfile% file for more details >> %logfile%
	set sendEmail=true

)

IF %errorsFound%==1 (

	echo errors found in response uploading files, see %ftpResponseFile% and %logfile% file for more details >> %logfile%
	set sendEmail=true

)

type %ftpResponseFile% >> %logfile%

IF %filesFound%==0 (

	echo error no files found to upload, see %ftpResponseFile% and %logfile% file for more details >> %logfile%
	set sendEmail=true

)

echo there were %errorsFound% errors found...
IF %errorsFound%==0 (

    echo moving sent files to backup directory >> %logfile%
    copy /y %workDir%\BinTagItem.txt* %backupDir%\BinTagItem.txt* & del /q %workDir%\BinTagItem.txt* >> %logfile%
    
)

	
echo error level %errorlevel% errors found %errorsFound% file found  %filesFound% sendemail %sendEmail% >> %logfile%



if "%sendEmail%" == "true" (
REM	
)

cd %startDir%  

goto END



:NOFILES
cd %startDir%
echo no files at this time!>> %logfile%
goto END

:END

echo done
echo get SendTolexmarkFTP ended at %date% %time% >> %logfile%
echo ********************************** >> %logfile%

ENDLOCAL

