@ECHO OFF
REM This script is used to send(FTP) the BinTagRegPrice.txt file to Lexmark server
SETLOCAL
call E:\lcbo\LCO_BinTag\BinTag.bat

set rootDir=E:\lcbo\LCO_BinTag
set startDir=%rootDir%
set logDir=%rootDir%\LOGS
set sendDir=%rootDir%\
set workDir=%rootDir%\
set backupDir=%rootDir%\ARCH

E:
cd %rootDir%

REM set logfile=%logDir%\SendTolexmarkFTP_regprice.%SCRIPT_DTTM%.log

echo ********************************** 
echo %date% %time%  SendTolexmarkFTP started 

REM uploading file to lexmark folder
set remoteDir=/
set fileName=BinTagRegPrice.txt
set remotefileName=ItemRegPrice_%SCRIPT_DTTM%.txt
set ftpResponseFile=%rootDir%\LOGS\sftplexmarkResponse_regprice.log
set ftpAPTFile=%rootDir%\LOGS\SFTPlexmark_regprice.log
set sftpServer=%lexmarkFTPServer%

REM CHECK if there are files to be sent

set sendFiles=%sendDir%\%fileName%
set cnt=0
set fileSent=
set filesFound=0
set errorsFound=0

if not exist "%sendDir%\%fileName%" echo %date%  %time% Error in FTP Reguar Price file, No file found & goto END

cd %rootDir% 

echo. > %ftpResponseFile%

set sftpURL=sftp://%ftpUID%:%ftpPWD%@%sftpServer% -hostkey=*
REM set sftpURL=sftp://%ftpUID%@%sftpServer% -privatekey=%privateKeyFile%

echo option batch abort >%ftpAPTFile%
echo option confirm off >>%ftpAPTFile%
echo open %sftpURL% >>%ftpAPTFile%
echo cd %remoteDir% >> %ftpAPTFile%
echo lcd %workDir% >> %ftpAPTFile%
echo bin >> %ftpAPTFile%
echo put  %FileName% %remotefileName% >> %ftpAPTFile%
echo close >> %ftpAPTFile%
echo exit >> %ftpAPTFile%

%rootDir%\winscp.com /script:%ftpAPTFile% > %ftpResponseFile%

copy /y %sendDir%\%fileName% %backupDir%\%remotefileName% & del /q %sendDir%\%fileName% 

REM findstr /i /c:"Error" %ftpResponseFile% 
REM If %errorlevel%==0 echo %date%  %time% Error in FTP Regular Price file

findstr /i /c:"Transfer" %ftpResponseFile% 
If %errorlevel%==1 echo %date%  %time% Error in FTP Regular Price file


goto END

:END

echo done
echo get SendTolexmarkFTP ended at %date% %time% 
echo ********************************** 

ENDLOCAL

