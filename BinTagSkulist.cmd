@echo off
setlocal

call E:\lcbo\LCO_BinTag\BinTag.bat

set CUR_DIR=%~dp0
REM REMOVE TRAILING BACKSLACH IF PRESENT
IF %CUR_DIR:~-1%==\ SET CUR_DIR=%CUR_DIR:~0,-1%

set LOG_DIR=%CUR_DIR%\LOGS
IF NOT EXIST %LOG_DIR% MKDIR %LOG_DIR%
set LOG_FILE=%LOG_DIR%\%~nx0_%SCRIPT_DATE%.log

echo %SCRIPT_DTTM% START %TIME% >> %LOG_FILE%

REM ***********MAIN CONTENT START***************

set DB=%ORACLE_SID%.lcbo.com

sqlplus %user%/%passwd%@%DB% @%CUR_DIR%\BinTagSkulistExtract.sql >> %LOG_FILE%

call %CUR_DIR%\BinTagSkulistFTP.bat >> %LOG_FILE%

GOTO END
REM ***********MAIN CONTENT END ****************

:STANDBY
echo %SCRIPT_DTTM% - Run on STANDBY. Nothing to Process. >> %LOG_FILE%

:END
echo %SCRIPT_DTTM% FINISH %TIME% >> %LOG_FILE%
endlocal

REM exit
