SPOOL BinTagPrice.txt
set term off
set feedback off
set heading off
set trimspool on
set pages 0
set linesize 500
set feed off
set echo off
set verify off
SELECT 'ITEMID|REGPRICE|SALEPRICE|STARTDATE|STOPDATE|DIFF|CHANGETYPE'
FROM DUAL
/
 SELECT 
            PR.ITEM_NUMBEr||'|'||
            to_char(nvl(prc.retl_prce_amt,0) + nvl(sl.approved,0))||'|'||
            prc.retl_prce_amt||'|'||
             to_char(SL.START_DATE,'YYYY-MM-DD')||'|'||
            to_char(SL.END_DATE,'YYYY-MM-DD')||'|'||
            SL.Approved||'|'||
            'SS'
        FROM pts_promotion_applications pr,
            pts_super_sale_lto sl,
            pts_promotions p,
            prcadmin.pricing_master@lcsg.world PRC,
          (  SELECT prc.item_no AS item_nO,
                    MAX (prc.EFCT_PRCE_DT) AS max_date
               FROM prcadmin.pricing_master@lcsg.world prc
              WHERE     prc.EFCT_PRCE_DT <= SYSDATE
                 and prc.item_type_cd = 'REG'
               GROUP BY prc.item_no) maxdate
      WHERE     PR.PROMOTION_APP_ID = SL.PROMOTION_APP_ID
            AND PR.PROMO_SSL_ID = P.PROMOTION_ID
            AND P.STATUS_CODE = 'A'
            AND TO_CHAR(SL.START_DATE,'YYYYMMDD') <= TO_CHAR(SYSDATE,'YYYYMMDD')  
            AND TO_CHAR(SL.END_DATE,'YYYYMMDD') >= TO_CHAR(SYSDATE,'YYYYMMDD')
            AND PR.item_number = PRC.item_no
            AND PRC.item_no = maxdate.item_no
            AND PRC.efct_prce_dt = maxdate.max_date
                   AND prc.item_type_cd = 'REG'
    UNION 
      SELECT 
          PA.ITEM_NUMBER||'|'||
          to_char(nvl(PRC.RETL_PRCE_AMT,0) + nvl(LT.APPROVED,0))||'|'||
          PRC.RETL_PRCE_AMT||'|'||
          TO_CHAR(LC.LTO_PERIOD_BEGIN,'YYYY-MM-DD')||'|'||
          TO_CHAR(LC.LTO_PERIOD_END,'YYYY-MM-DD')||'|'||
          LT.APPROVED||'|'||
          'LTO'
      FROM pts_promotion_applications pa,
          pts_promotions pr,
          PTS_LIMITED_TIME_OFFER lt,
          PTS_INV_LTO_CALENDAR lc,
          prcadmin.pricing_master@lcsg.world PRC,
          (  SELECT prc.item_no AS item_nO,
                    MAX (prc.EFCT_PRCE_DT) AS max_date
               FROM prcadmin.pricing_master@lcsg.world prc
              WHERE     prc.EFCT_PRCE_DT <= SYSDATE
                 and prc.item_type_cd = 'REG'
               GROUP BY prc.item_no) maxdate
    WHERE     PA.PROMO_LTO_ID = PR.PROMOTION_ID
          AND PR.PROMOTION_ID = LT.PROMOTION_ID
          AND Pa.PROMOTION_PERIOD = LC.PROMOTION_PERIOD
          --AND I.ITEM_NUMBER = PA.ITEM_NUMBER
          AND PA.STATUS_CODE = 'A'
          AND PR.STATUS_CODE = 'A'
          AND TO_CHAR(LC.LTO_PERIOD_BEGIN,'YYYYMMDD') <= TO_CHAR(SYSDATE,'YYYYMMDD')  
          AND TO_CHAR(LC.LTO_PERIOD_END,'YYYYMMDD') >= TO_CHAR(SYSDATE,'YYYYMMDD')
          AND PA.ITEM_NUMBER = PRC.ITEM_NO
          AND PRC.ITEM_NO = MAXDATE.ITEM_NO
          AND PRC.EFCT_PRCE_DT = MAXDATE.MAX_DATE
          AND PRC.ITEM_TYPE_CD = 'REG'
/
SPOOL OFF
/
quit

